using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndScreenUI : UIScreen
{
    public GameObject WinContainer;
    public GameObject LoseContainer;

    private void OnEnable()
    {
        if (GameManager.Instance.GetGameState() == GameState.PlayerWon)
        {
            WinContainer.SetActive(true);
        }
        else if (GameManager.Instance.GetGameState() == GameState.PlayerLost)
        {
            LoseContainer.SetActive(true);
        }
    }

    private void OnDisable()
    {
        WinContainer.SetActive(false);
        LoseContainer.SetActive(false);
    }
}
