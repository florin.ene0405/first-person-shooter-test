using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InGameScreenUI : UIScreen
{
    private void OnEnable()
    {
        GameManager.Instance.PauseGame();
        UnlockMouse();
    }

    public void CloseInGameScreen()
    {
        UIManager.Instance.DisableAllUI(true);
    }

    public void RestartGame()
    {
        GameManager.Instance.RestartGame();
    }

    public void GoBackToMainMenu()
    {
        SceneManager.LoadScene(0);
    }

    private void OnDisable()
    {
        GameManager.Instance.UnPauseGame();
        LockMouse();
    }

    private void UnlockMouse()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    private void LockMouse()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }
}
