using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChooseWeponScreenUI : UIScreen
{
    public WeponSelection Wepon1;
    public WeponSelection Wepon2;

    private LevelScriptableObject _currentLevel;

    private void OnEnable()
    {
        _currentLevel = GameManager.Instance.GetCurrentLevel();

        Wepon1.OnServerInitialized(_currentLevel.FirstGun);
        Wepon2.OnServerInitialized(_currentLevel.SecondGun);
    }

    public void OpenIntroScreen()
    {
        UIManager.Instance.ShowUIPanel("IntroScreen");
    }

    public void OnGunSellected(GunScriptableObject gun)
    {
        GameManager.Instance.OnGunSellected(gun);

        UIManager.Instance.DisableAllUI(true);
    }
}
