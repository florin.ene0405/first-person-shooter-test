using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroScreenUI : UIScreen
{
    public GameObject ExitPanel;

    public void OpenTutorialScreen()
    {
        UIManager.Instance.ShowUIPanel("TutorialScreen");
    }

    public void OpenChooseWekonScreen()
    {
        UIManager.Instance.ShowUIPanel("ChooseWeponScreen");
    }

    public void ShowExitPanel()
    {
        if (ExitPanel == null)
        {
            Debug.LogError("ExitPanel is null");
            return;
        }

        ExitPanel.SetActive(true);
    }

    public void CloseExitPanel()
    {
        if (ExitPanel == null)
        {
            Debug.LogError("ExitPanel is null");
            return;
        }

        ExitPanel.SetActive(false);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
