using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    #region Singleton
    public static GameManager Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }
    #endregion

    public LevelManagerScriptableObjec Levels;
    public ActorController PlayerPrefab;

    private int _currentLevel = 0;
    private GunScriptableObject _currentGun;
    private MapController _map;
    private Camera _camera;
    private bool _isGamePaused;
    private ActorController _player;
    private int _enemiesAlive;
    private int _numbersOfLevels;
    private GameState _gameState;

    private void Start()
    {
        _camera = Camera.main;
        _numbersOfLevels = Levels.Levels.Count;
    }

    public LevelScriptableObject GetCurrentLevel() => Levels.Levels[_currentLevel];

    public void OnGunSellected(GunScriptableObject gun)
    {
        _currentGun = gun;
        CreateTheLevel();
    }

    private void CreateTheLevel()
    {
        _gameState = GameState.Playng;

        if (_map != null)
        {
            Destroy(_map.gameObject);
        }

        _camera.gameObject.SetActive(false);

        _map = Instantiate<MapController>(GetCurrentLevel().Map, Vector3.zero, Quaternion.identity);

        _map.Initialize(GetCurrentLevel(), PlayerPrefab);

        _player = _map.GetPlayer();
        _player.OnGunInitialize(_currentGun);
        _enemiesAlive = GetCurrentLevel().NumberOfEnemies;
    }

    public void OnEnemiKilled(ActorController actorController)
    {
        if (actorController.IsPlayer)
        {
            _gameState = GameState.PlayerLost;
            GameOver();
        }

        _enemiesAlive--;

        if (_enemiesAlive == 0)
        {
            _gameState = GameState.PlayerWon;
            OnLevelOver();
        }
    }

    private void OnLevelOver()
    {
        _map.ShowDoor();
    }

    public void GoToTheNextLevel()
    {
        _currentLevel++;

        if (_currentLevel >= Levels.Levels.Count)
        {
            GameOver();
            return;
        }

        UIManager.Instance.ShowUIPanel("ChooseWeponScreen");
    }

    private void GameOver()
    {
        UIManager.Instance.ShowUIPanel("EndScreen");
        _camera.gameObject.SetActive(true);
        Destroy(_map.gameObject);
    }

    public void RestartGame()
    {
        CreateTheLevel();
        UIManager.Instance.DisableAllUI(true);
    }

    public void PauseGame()
    {
        _isGamePaused = true;
    }

    public void UnPauseGame()
    {
        _isGamePaused = false;
    }

    public bool IsGamePaused() => _isGamePaused;

    public ActorController GetPlayer() => _player;

    public GameState GetGameState() => _gameState;
}

public enum GameState
{
    None = 0,
    Playng = 1,
    PlayerWon = 2,
    PlayerLost = 3
}
