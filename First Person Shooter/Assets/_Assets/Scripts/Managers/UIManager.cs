using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    #region Singleton
    public static UIManager Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }
    #endregion

    public List<UIScreen> AllUIs = new List<UIScreen>();
    public RectTransform Target;
    public Image Background;

    private UIScreen _openUI;
    private const string INTRO_SCREEN = "IntroScreen";

    private void Start()
    {
        DisableAllUI();
        ShowUIPanel(INTRO_SCREEN);
    }

    public void ShowUIPanel(string name)
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;

        if (!Background.gameObject.activeInHierarchy)
        {
            Background.gameObject.SetActive(true);
        }

        foreach (var uis in AllUIs)
        {
            if (string.Equals(uis.Name, name))
            {
                if (_openUI != null)
                {
                    _openUI.gameObject.SetActive(false);
                }

                uis.gameObject.SetActive(true);
                _openUI = uis;
                return;
            }
        }

        Debug.LogWarning("No UI with name of: " + name);
    }

    public void ShowInGameScreen()
    {
        ShowUIPanel("InGameScreen");
    }

    public void DisableAllUI(bool includingBackground = false)
    {
        AllUIs.ForEach(ui => ui.gameObject.SetActive(false));

        if (includingBackground)
        {
            Background.gameObject.SetActive(false);
        }
    }
}
