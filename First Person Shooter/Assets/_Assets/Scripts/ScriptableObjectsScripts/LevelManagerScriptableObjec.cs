using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelManagerScriptableObjec", menuName = "ScriptableObjects/LevelManagerScriptableObjec", order = 1)]
public class LevelManagerScriptableObjec : ScriptableObject
{
    public List<LevelScriptableObject> Levels = new List<LevelScriptableObject>();
}
