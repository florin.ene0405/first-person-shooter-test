using UnityEngine;

[CreateAssetMenu(fileName = "GunScriptableObject", menuName = "ScriptableObjects/GunScriptableObject", order = 3)]
public class GunScriptableObject : ScriptableObject
{
    [Header("Gun Prefab")]
    public GameObject Gun;
    
    [Header("Gun Sprite")]
    public Sprite GunImage;

    [Header("Gun Stats")]
    public string Name;
    public GunType GunType;
    public int Damage;
    public int ClipSize;
    public int Ammo;
    public float ReloadTime;
    public float RateOfFire;
}

public enum GunType
{
    None = 0,
    HandGun = 1,
    Riffle = 2
}