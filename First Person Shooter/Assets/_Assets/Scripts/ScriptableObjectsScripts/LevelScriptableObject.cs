using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelScriptableObject", menuName = "ScriptableObjects/LevelScriptableObject", order = 2)]
public class LevelScriptableObject : ScriptableObject
{
    [Header("Guns")]
    public GunScriptableObject FirstGun;
    public GunScriptableObject SecondGun;

    [Header("Enemies")]
    [Range(1,10)]
    public int NumberOfEnemies;

    [Header("Actors Prefabs")]
    public List<ActorController> ActorsPrefabs = new List<ActorController>();

    [Header("Map")]
    public MapController Map;
}
