using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapController : MonoBehaviour
{
    public List<Transform> SpawnPowints = new List<Transform>();
    public Transform PlayerSpawnPowints;
    public GameObject Door;

    private ActorController _player;

    private void Start()
    {
        Door.SetActive(false);
    }

    internal void Initialize(LevelScriptableObject levelScriptableObject, ActorController player)
    {
        _player = Instantiate<ActorController>(player, PlayerSpawnPowints.position, Quaternion.identity, transform);

        for (int i = 0; i < levelScriptableObject.NumberOfEnemies; i++)
        {
            int randomSpawn = UnityEngine.Random.Range(0, SpawnPowints.Count);
            int randomAgent = UnityEngine.Random.Range(0, levelScriptableObject.ActorsPrefabs.Count);

            Instantiate<ActorController>(levelScriptableObject.ActorsPrefabs[randomAgent], SpawnPowints[randomSpawn].position, Quaternion.identity, transform);

            SpawnPowints.RemoveAt(randomSpawn);
        }
    }

    public ActorController GetPlayer() => _player;

    public void ShowDoor()
    {
        Door.SetActive(true);
    }
}
