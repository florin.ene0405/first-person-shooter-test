using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class WeponSelection : MonoBehaviour
{
    public ChooseWeponScreenUI ChooseWeponScreenUI;
    public Image Image;
    public TextMeshProUGUI Name; 
    public TextMeshProUGUI Status;

    private GunScriptableObject _myGun;

    public void OnServerInitialized(GunScriptableObject myGun)
    {
        _myGun = myGun;

        Image.sprite = _myGun.GunImage;

        Name.text = _myGun.Name;

        Status.text = $" Damage: {_myGun.Damage} \n ClipSize: {_myGun.ClipSize} \n Ammo: {_myGun.Ammo} \n FireRate: {_myGun.RateOfFire}";
    }

    public void OnGunSellected()
    {
        ChooseWeponScreenUI.OnGunSellected(_myGun);
    }
}
