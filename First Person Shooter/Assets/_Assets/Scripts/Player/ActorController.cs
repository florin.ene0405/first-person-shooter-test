using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class ActorController : MonoBehaviour
{
    [Header("Player")]
    public bool IsPlayer;
    public bool IsAlive = true;
    public int Life = 100;

    [Header("Speeds")]
    public float WalkSpeed;
    public float RunSpeed;
    public float JumpForce;
    public float LookSpeed;

    [Header("Gun")]
    public GameObject GunSpawn;
    public GameObject Gun;

    [Header("Particle")]
    public ParticleSystem BloodParticle;

    [Header("Sounds")]
    public AudioClip FootstepsSound;
    public AudioClip JumpSound;
    public AudioClip PistolSound;

    public Transform CameraTransform;

    private CharacterController _controller;
    private CapsuleCollider _capsuleCollider;
    private float _rotationX = 0f;
    private float _verticalVelocity = 0f;
    private float _jumpDelay = 0.5f;
    private float _jumpTimer = 0;

    private int _weponDamage = 30;
    private float _rateOfFire;
    private float _lastBulletShoot;
    private float _reloadTimer;

    private ActorController _player;
    private float _aITimer = 1f;
    private float _aICooldown;
    private int _aIPercentageToShoot = 30;

    void Start()
    {
        _controller = GetComponent<CharacterController>();
        _capsuleCollider = gameObject.AddComponent<CapsuleCollider>();

        if (!IsPlayer)
        {
            _controller.enabled = false;
            _player = GameManager.Instance.GetPlayer();
            return;
        }

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    void Update()
    {
        if (!_capsuleCollider.enabled && GameManager.Instance.IsGamePaused() && IsPlayer)
        {
            _capsuleCollider.enabled = true;
        }

        if (_capsuleCollider.enabled && !GameManager.Instance.IsGamePaused() && IsPlayer)
        {
            _capsuleCollider.enabled = false;
        }

        if (!GameManager.Instance.IsGamePaused() && !IsPlayer && IsAlive)
        {
            AIController();
        }

        if (GameManager.Instance.IsGamePaused() || !IsAlive || !IsPlayer)
        {
            return;
        }

        if (_controller == null)
        {
            Debug.LogError("CharacterController is null in " + this);
            return;
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            UIManager.Instance.ShowInGameScreen();
        }


        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");
        Vector3 moveDirection = new Vector3(horizontalInput, 0f, verticalInput).normalized;

        float moveSpeed = Input.GetKey(KeyCode.LeftShift) ? RunSpeed / 100 : WalkSpeed / 100;

        Vector3 move = transform.TransformDirection(moveDirection) * moveSpeed;
        _controller.Move(move * Time.deltaTime);

        _jumpTimer -= Time.deltaTime;
        if (Input.GetButtonDown("Jump") && _jumpTimer < 0)
        {
            _verticalVelocity = JumpForce;
            _jumpTimer = _jumpDelay;
            PlayAudio(JumpSound);
        }

        _verticalVelocity += Physics.gravity.y * 10 * Time.deltaTime;
        move.y = _verticalVelocity * Time.deltaTime;

        _controller.Move(move);

        float mouseX = Input.GetAxis("Mouse X") * LookSpeed;
        float mouseY = Input.GetAxis("Mouse Y") * LookSpeed;

        _rotationX -= mouseY;
        _rotationX = Mathf.Clamp(_rotationX, -90f, 90f);

        CameraTransform.localRotation = Quaternion.Euler(_rotationX, 0f, 0f);
        transform.Rotate(Vector3.up * mouseX);


        _lastBulletShoot += Time.deltaTime;
        if (Input.GetMouseButtonDown(0) && _lastBulletShoot >= _rateOfFire)
        {
            _lastBulletShoot = 0;
            Shoot();
        }
    }

    private void AIController()
    {
        transform.LookAt(_player.transform);
        _aICooldown += Time.deltaTime;

        if (_aICooldown >= _aITimer)
        {
            _aICooldown = 0;

            int randon = UnityEngine.Random.Range(0, 100);

            if (_aIPercentageToShoot <= randon)
            {
                AIShoot();
                PlayShotAnimation();
                PlayAudio(PistolSound);
            }
        }
    }

    private void AIShoot()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit))
        {
            ActorController enemy;

            hit.transform.TryGetComponent<ActorController>(out enemy);

            if (enemy != null && enemy.IsPlayer)
            {
                var blood = Instantiate<ParticleSystem>(BloodParticle, hit.point, Quaternion.identity);
                DestroyParticleSystem(blood);

                enemy.TakeDamake(_weponDamage);
            }
        }
    }

    public void OnGunInitialize(GunScriptableObject gun)
    {
        _weponDamage = gun.Damage;
        _rateOfFire = gun.RateOfFire;
        _reloadTimer = gun.ReloadTime;

        Vector3 oldPosition = Gun.transform.position;

        if (Gun != null)
        {
            Destroy(Gun.gameObject);
        }

        Gun = Instantiate<GameObject>(gun.Gun, oldPosition, Quaternion.identity, GunSpawn.transform);
        Gun.transform.rotation = new Quaternion(0, 0, 0, 0);
    }

    private void Shoot()
    {
        ShotRaycast();
        PlayShotAnimation();
        PlayAudio(PistolSound);
    }

    private void ShotRaycast()
    {
        Vector3 screenCenter = new Vector3(Screen.width / 2f, Screen.height / 2f, 0f);

        Ray ray = Camera.main.ScreenPointToRay(screenCenter);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            Debug.Log("Hit object: " + hit.collider.gameObject.name);
            Debug.Log("Hit point: " + hit.point);

            ActorController enemy;

            hit.transform.TryGetComponent<ActorController>(out enemy);

            if (enemy != null)
            {
                Debug.Log("Enemy hit: " + enemy.transform.name);
                var blood = Instantiate<ParticleSystem>(BloodParticle, hit.point, Quaternion.identity);
                DestroyParticleSystem(blood);

                enemy.TakeDamake(_weponDamage);
            }
        }
    }

    private async void DestroyParticleSystem(ParticleSystem ps)
    {
        await Task.Delay(1000);
        Destroy(ps.gameObject);
    }

    private void PlayShotAnimation()
    {
        transform.DOShakeRotation(0.1f, Vector3.right, 30);
        Gun.transform.DOShakeRotation(0.1f, Vector3.forward * 5, 200);
    }

    private void PlayAudio(AudioClip audioToBePlayed)
    {
        if (audioToBePlayed == null)
        {
            Debug.LogWarning("The audio clip is not ");
            return;
        }

        AudioSource.PlayClipAtPoint(audioToBePlayed, transform.position);
    }

    public void TakeDamake(int damage)
    {
        if (!IsAlive)
        {
            return;
        }

        Life -= damage;

        if (Life <= 0)
        {
            IsAlive = false;
            GameManager.Instance.OnEnemiKilled(this);
            PlayDeathAnimation();
        }
    }

    private async void PlayDeathAnimation()
    {
        CapsuleCollider capsule;

        TryGetComponent<CapsuleCollider>(out capsule);

        if (capsule != null)
        {
            capsule.enabled = false;
        }

        await Task.Delay(5000);

        if (gameObject != null)
        {
            Destroy(gameObject);
        }
    }
}
