using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorTrigger : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        ActorController player;

        other.gameObject.TryGetComponent<ActorController>(out player);

        if (player != null && player.IsPlayer)
        {
            GameManager.Instance.GoToTheNextLevel();
        }
    }
}
